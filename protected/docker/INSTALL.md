##INSTALLATION INSTRUCTION

For install project by Docker, you must correct install docker-compose package. More info about the installation is at url: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

***Before start installation stop all XDebug listeners !!!***

#1. Create Docker containers
Execute next command:

`docker-compose -p mordor up` - first start

`docker-compose -p mordor up -d` - deamon start

`docker-compose -p mordor down` - down project

#2. Docker commands

`docker-compose -p mordor exec --user 1000 app /bin/bash`

`docker-compose -p mordor build`