<?php

namespace Interfaces;

interface IApplication
{

    public function run(IRequest $request);

}