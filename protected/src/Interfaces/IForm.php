<?php
/**
 * Created by PhpStorm.
 * User: mariyo
 * Date: 9.4.2018
 * Time: 15:42
 */

namespace Interfaces;


interface IForm
{
    public function validate(): array;

    public function values(): array;

    public function isSubmitted(): bool;
}