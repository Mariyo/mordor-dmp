<?php

namespace Interfaces;

interface IResponse
{

    public function setBody(string $body);

    public function getBody(): string;

    public function sendHeaders();

    public function sendContent();

}