<?php

namespace Interfaces;

interface IAction
{

    public function run() : IResponse;

}