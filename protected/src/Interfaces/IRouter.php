<?php

namespace Interfaces;

interface IRouter
{

    public function route(IRequest $request): string;

}