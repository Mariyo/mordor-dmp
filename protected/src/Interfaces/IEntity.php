<?php
/**
 * Created by PhpStorm.
 * User: mariyo
 * Date: 9.4.2018
 * Time: 15:42
 */

namespace Interfaces;


interface IEntity
{
    public function setValues(array $values);
}