<?php

namespace Interfaces;

interface IRequest
{
    public function getUri();
}