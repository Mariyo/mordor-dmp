<?php

namespace Dao;

use PDO;

abstract class Base
{

    protected $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

}