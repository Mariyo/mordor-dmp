<?php

namespace Dao;

use Entity;
use Exception;
use PDO;

class Creature extends Base
{
    /**
     *
     */
    const GENDERS = [
        1 => 'Male',
        2 => 'Female',
        3 => 'Unknown',
        4 => 'Other'
    ];

    /**
     *
     */
    const RACES = [
        1 => 'Elf',
        2 => 'Dwarf',
        3 => 'Hobbit',
        4 => 'Orc',
        5 => 'Human',
        6 => 'Ghost',
        7 => 'Other'
    ];

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'creatures';
    }

    /**
     * @param $id
     * @return Entity\Creature
     */
    public function getById($id): Entity\Creature
    {
        $stmt = $this->db->prepare("SELECT * FROM {$this->getTableName()} WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $creature = $stmt->fetchObject(Entity\Creature::class);
        $stmt->closeCursor();
        return $creature;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function delete($id)
    {
        $this->db->beginTransaction();

        try {
            $stmt = $this->db->prepare("DELETE FROM {$this->getTableName()} WHERE id = :id");
            $stmt->execute(['id' => (int)$id]);

            if ($stmt->errorCode() !== '00000') {
                throw new Exception(implode("\n", $stmt->errorInfo()));
            }

            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }

        return true;
    }

    /**
     * @param null $extra
     * @return Entity\Creature[]
     */
    public function findAll($extra = null): array
    {
        $stmt = $this->db->query("SELECT * FROM {$this->getTableName()} " . $extra);
        $stmt->execute();
        $creature = $stmt->fetchAll(PDO::FETCH_CLASS, Entity\Creature::class);
        $stmt->closeCursor();
        return $creature;
    }

    /**
     * @param Entity\Creature $creature
     * @return bool
     * @throws Exception
     */
    public function create(Entity\Creature $creature): bool
    {
        $this->db->beginTransaction();

        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->getTableName()}
              (name, gender, birthplace, birthdate, carrier, enslaved, race) VALUES
              (:name, :gender, :birthplace, :birthdate, :carrier, :enslaved, :race)");

            $stmt->execute([
                'name' => $creature->name,
                'gender' => $creature->gender,
                'birthplace' => $creature->birthplace,
                'birthdate' => $creature->birthdate,
                'carrier' => $creature->carrier,
                'enslaved' => $creature->enslaved,
                'race' => $creature->race
            ]);

            if ($stmt->errorCode() !== '00000') {
                throw new Exception(implode("\n", $stmt->errorInfo()));
            }

            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }

        return true;
    }

    /**
     * @param Entity\Creature $creature
     * @return bool
     * @throws Exception
     */
    public function update(Entity\Creature $creature): bool
    {
        $this->db->beginTransaction();

        try {
            $stmt = $this->db->prepare("UPDATE {$this->getTableName()} SET
              name = :name, 
              gender = :gender,
              birthplace = :birthplace,
              birthdate = :birthdate, 
              carrier = :carrier,
              enslaved = :enslaved,
              race = :race
              WHERE id = :id
            ");

            $stmt->execute([
                'name' => $creature->name,
                'gender' => $creature->gender,
                'birthplace' => $creature->birthplace,
                'birthdate' => $creature->birthdate,
                'carrier' => $creature->carrier,
                'enslaved' => $creature->enslaved,
                'race' => $creature->race,
                'id' => $creature->id
            ]);

            if ($stmt->errorCode() !== '00000') {
                throw new Exception(implode("\n", $stmt->errorInfo()));
            }

            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }

        return true;
    }

}