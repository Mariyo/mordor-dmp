<?php

namespace Dao;

use Entity;
use Exception;
use PDO;

class Note extends Base
{

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'notes';
    }

    /**
     * @param null $extra
     * @return Entity\Note[]
     */
    public function findAll($extra = null): array
    {
        $stmt = $this->db->query("SELECT * FROM {$this->getTableName()} " . $extra);
        $stmt->execute();
        $notes = $stmt->fetchAll(PDO::FETCH_CLASS, Entity\Note::class);
        $stmt->closeCursor();
        return $notes;
    }

    /**
     * @param Entity\Note $note
     * @return bool
     * @throws Exception
     */
    public function create(Entity\Note $note): bool
    {
        $this->db->beginTransaction();

        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->getTableName()}
              (note, date, creature_id) VALUES
              (:note, NOW(), :creature_id)");

            $stmt->execute([
                'note' => $note->note,
                'creature_id' => $note->creature_id,
            ]);

            if ($stmt->errorCode() !== '00000') {
                throw new Exception(implode("\n", $stmt->errorInfo()));
            }

            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }

        return true;
    }

}