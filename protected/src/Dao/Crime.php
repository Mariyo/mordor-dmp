<?php

namespace Dao;

use Entity;
use Exception;
use PDO;

class Crime extends Base
{

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'crimes';
    }

    /**
     * @param null $extra
     * @return Entity\Crime[]
     */
    public function findAll($extra = null): array
    {
        $stmt = $this->db->query("SELECT * FROM {$this->getTableName()} " . $extra);
        $stmt->execute();
        $crimes = $stmt->fetchAll(PDO::FETCH_CLASS, Entity\Crime::class);
        $stmt->closeCursor();
        return $crimes;
    }

    /**
     * @param Entity\Crime $crime
     * @return bool
     * @throws Exception
     */
    public function create(Entity\Crime $crime): bool
    {
        $this->db->beginTransaction();

        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->getTableName()}
              (note, punished, date, creature_id) VALUES
              (:note, :punished, NOW(), :creature_id)");

            $stmt->execute([
                'note' => $crime->note,
                'punished' => $crime->punished,
                'creature_id' => $crime->creature_id,
            ]);

            if ($stmt->errorCode() !== '00000') {
                throw new Exception(implode("\n", $stmt->errorInfo()));
            }

            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }

        return true;
    }

}