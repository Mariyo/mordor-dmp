<?php

class RedirectResponse extends Response implements Interfaces\IResponse
{

    /**
     * @var string
     */
    protected $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return $this|void
     */
    public function sendHeaders()
    {
        header('Location: ' . $this->path);
        exit();
    }

}