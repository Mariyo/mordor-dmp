<?php

namespace Entity;

use DateTime;

class Creature extends Base
{

    /**
     * Up to 60 chars...
     * @var string
     */
    public $name;

    /**
     * Male,Female,Unknown,Other
     * @var array
     */
    public $gender;

    /**
     * Up to 50 chars...
     * @var string
     */
    public $birthplace;

    /**
     * @var DateTime
     */
    public $birthdate;

    /**
     * Ever carried The Ring?
     * @var boolean
     */
    public $carrier;

    /**
     * @var boolean
     */
    public $enslaved;

    /**
     * Elf, Dwarf, Hobbit, Orc, Human, Ghost, Other
     * @var array
     */
    public $race;

}