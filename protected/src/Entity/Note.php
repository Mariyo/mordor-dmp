<?php

namespace Entity;

use DateTime;

class Note extends Base
{

    /**
     * Up to 10000 chars...
     * @var string
     */
    public $note;

    /**
     * @var DateTime
     */
    public $date;

    /**
     * @var int
     */
    public $creature_id;

}