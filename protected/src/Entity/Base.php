<?php

namespace Entity;

use Interfaces\IEntity;

abstract class Base implements IEntity
{
    public $id;

    public function setValues(array $values)
    {
        foreach ($values as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
}