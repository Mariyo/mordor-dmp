<?php

namespace Entity;

use DateTime;

class Crime extends Base
{

    /**
     * Up to 10000 chars...
     * @var string
     */
    public $note;

    /**
     * @var boolean
     */
    public $punished;

    /**
     * @var DateTime
     */
    public $date;

    /**
     * @var int
     */
    public $creature_id;

}