<?php

namespace Action\Creature;

use Action\Base;
use Entity\Creature;
use Exception;
use Form;
use Interfaces\IResponse;
use RedirectResponse;
use Response;


class Create extends Base
{

    const PASSWORD = '$2y$10$aNkr7n7Zn.V/J8rj8UM89OgsxM7kpB8wQuHbFc.M8z8zqGW1U6VjK';

    /**
     * @return IResponse
     */
    public function run(): IResponse
    {
        if (!$this->passwordCheck('password_create')) {
            return $this->askForPassword();
        }

        $form = new Form\Creature\Create();
        if ($form->isSubmitted()) {
            $errors = $form->validate();
            if (empty($errors)) {
                $creature = new Creature();
                $creature->setValues($form->values());
                $creatureDao = new \Dao\Creature($this->pdo);

                try {
                    $creatureDao->create($creature);
                    unset($_SESSION['password_create']);
                    return new RedirectResponse('/');
                } catch (Exception $e) {
                    $errors = [$e->getMessage()];
                }
            }
        }

        $layout = $this->view('protected/templates/layouts/default.php', [
            'title' => 'Creating creature',
            'content' => $this->view('protected/templates/views/creature/create.php'),
            'errors' => isset($errors) ? $errors : null,
        ]);

        return $this->response($layout);
    }

    /**
     * @return RedirectResponse|Response
     */
    protected function askForPassword()
    {
        if (isset($_POST['password'])) {
            if (password_verify($_POST['password'], static::PASSWORD)) {
                $_SESSION['password_create'] = static::PASSWORD;
            }

            return new RedirectResponse('/creature/create');
        }

        $layout = $this->view('protected/templates/layouts/password.php', [
            'title' => 'Password for Creation of Creature',
        ]);

        return $this->response($layout);
    }

}