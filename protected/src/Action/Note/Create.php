<?php

namespace Action\Note;

use Action\Base;
use Entity\Note;
use Exception;
use Form;
use Interfaces\IResponse;
use RedirectResponse;

class Create extends Base
{

    /**
     * @return IResponse
     */
    public function run(): IResponse
    {
        $form = new Form\Note\Create();
        if ($form->isSubmitted()) {
            $errors = $form->validate();
            if (empty($errors)) {
                $note = new Note();
                $note->setValues($form->values());
                $note->creature_id = isset($_GET['creature']) ? $_GET['creature'] : null;
                $noteDao = new \Dao\Note($this->pdo);

                try {
                    $noteDao->create($note);
                    return new RedirectResponse(isset($_GET['redirect']) ? ('/?' . $_GET['redirect']) : '/');
                } catch (Exception $e) {
                    $errors = [$e->getMessage()];
                }
            }
        }

        $layout = $this->view('protected/templates/layouts/default.php', [
            'title' => 'Creating crime',
            'content' => $this->view('protected/templates/views/note/create.php'),
            'errors' => isset($errors) ? $errors : null,
        ]);

        return $this->response($layout);
    }

}