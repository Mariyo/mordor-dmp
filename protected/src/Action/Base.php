<?php

namespace Action;

use Exception;
use Interfaces\IAction;
use Interfaces\IRequest;
use PDO;
use Response;

abstract class Base implements IAction
{

    /**
     * This should be replaced by Dependency injection in future.
     * @var IRequest
     */
    protected $request;

    /**
     * This should be replaced by Dependency injection in future.
     * @var PDO
     */
    protected $pdo;

    public function __construct(IRequest $request, PDO $pdo)
    {
        $this->request = $request;
        $this->pdo = $pdo;
    }

    /**
     * @param string $path
     * @param array|null $variables
     * @return bool|string
     */
    protected function view(string $path, array $variables = [])
    {
        try {
            if (!is_readable($path)) {
                throw new Exception("View $path not found!", 1);
            }

            ob_start() && extract($variables, EXTR_SKIP);
            include $path;
            $content = ob_get_clean();
            ob_flush();
            return $content;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param string $body
     * @return Response
     */
    protected function response(string $body)
    {
        $response = new Response();
        $response->setBody($body);
        return $response;
    }

    /**
     * @return bool
     */
    protected function passwordCheck($type)
    {
        if (empty($_SESSION[$type])) {
            return false;
        }

        if ($_SESSION[$type] !== static::PASSWORD) {
            return false;
        }

        return true;
    }

}