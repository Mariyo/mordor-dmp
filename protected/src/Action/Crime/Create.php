<?php

namespace Action\Crime;

use Action\Base;
use Entity\Crime;
use Exception;
use Form;
use Interfaces\IResponse;
use RedirectResponse;

class Create extends Base
{

    /**
     * @return IResponse
     */
    public function run(): IResponse
    {
        $form = new Form\Crime\Create();
        if ($form->isSubmitted()) {
            $errors = $form->validate();
            if (empty($errors)) {
                $crime = new Crime();
                $crime->setValues($form->values());
                $crime->creature_id = isset($_GET['creature']) ? $_GET['creature'] : null;
                $crimeDao = new \Dao\Crime($this->pdo);

                try {
                    $crimeDao->create($crime);
                    return new RedirectResponse(isset($_GET['redirect']) ? ('/?' . $_GET['redirect']) : '/');
                } catch (Exception $e) {
                    $errors = [$e->getMessage()];
                }
            }
        }

        $layout = $this->view('protected/templates/layouts/default.php', [
            'title' => 'Creating crime',
            'content' => $this->view('protected/templates/views/crime/create.php'),
            'errors' => isset($errors) ? $errors : null,
        ]);

        return $this->response($layout);
    }

}