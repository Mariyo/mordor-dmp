<?php

namespace Action;

use Dao\Creature;
use Dao\Crime;
use Dao\Note;
use Exception;
use Interfaces\IResponse;
use RedirectResponse;

class Welcome extends Base
{

    const PASSWORD = '$2y$10$f/nwKzEiWkbc9GA2KZzhp..QCsPAQ.Cd8114tr34zq15keJo.EVdu';

    /**
     * @return IResponse
     */
    public function run(): IResponse
    {
        if (!$this->passwordCheck('password_list')) {
            return $this->askForPassword();
        }

        if (isset($_GET['remove'])) {
            $creatureRemoveDao = new Creature($this->pdo);
            try {
                $creatureRemoveDao->delete($_GET['remove']);
                unset($_GET['remove']);
                $query = http_build_query($_GET);
                return new RedirectResponse('?' . $query);
            } catch (Exception $e) {
                $errors = [$e->getMessage()];
            }
        }

        $extra = ' WHERE 1 ';

        if (isset($_GET['race']) and array_key_exists($_GET['race'], Creature::RACES)) {
            $extra .= ' AND race = ' . $_GET['race'];
        }

        if (!empty($_GET['crimes'])) {
            $extra .= ' AND id IN (SELECT creature_id FROM crimes WHERE punished = 0 GROUP BY creature_id HAVING COUNT(punished) >= ' . (int)$_GET['crimes'] . ')';
        }

        if (isset($_GET['orderby']) and in_array($_GET['orderby'], ['id', 'name', 'birthdate'])) {
            $extra .= ' ORDER BY ' . $_GET['orderby'];
            $extra .= ' ' . ((isset($_GET['order']) and $_GET['order'] == 'desc') ? 'desc' : 'asc');
        }

        $creatureDao = new Creature($this->pdo);
        $creatures = $creatureDao->findAll($extra);

        $crimeDao = new Crime($this->pdo);
        $crimes = $crimeDao->findAll();

        $crimesForCreatures = [];
        foreach ($crimes as $crime) {
            $crimesForCreatures[$crime->creature_id][] = $crime;
        }

        $noteDao = new Note($this->pdo);
        $notes = $noteDao->findAll();

        $notesForCreatures = [];
        foreach ($notes as $note) {
            $notesForCreatures[$note->creature_id][] = $note;
        }

        $layout = $this->view('protected/templates/layouts/default.php', [
            'title' => 'Creatures',
            'content' => $this->view('protected/templates/views/list.php', [
                'creatures' => $creatures,
                'orderby' => isset($_GET['orderby']) ? $_GET['orderby'] : null,
                'order' => isset($_GET['order']) ? $_GET['order'] : null,
                'raceSelected' => isset($_GET['race']) ? $_GET['race'] : null,
                'crimesSelected' => isset($_GET['crimes']) ? (int)$_GET['crimes'] : null,
                'redirect' => http_build_query($_GET, null, '&amp;'),
                'crimes' => $crimesForCreatures,
                'notes' => $notesForCreatures,
            ]),
            'errors' => isset($errors) ? $errors : null,
        ]);

        return $this->response($layout);
    }

    /**
     * @return RedirectResponse|Response
     */
    protected function askForPassword()
    {
        if (isset($_POST['password'])) {
            if (password_verify($_POST['password'], static::PASSWORD)) {
                $_SESSION['password_list'] = static::PASSWORD;
            }

            return new RedirectResponse('/');
        }

        $layout = $this->view('protected/templates/layouts/password.php', [
            'title' => 'Password for list of Creatures',
        ]);

        return $this->response($layout);
    }

}