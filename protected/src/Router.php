<?php

class Router implements Interfaces\IRouter
{

    /**
     * @param \Interfaces\IRequest $request
     * @return \Interfaces\IAction
     */
    public function route(Interfaces\IRequest $request): string
    {
        $uri = $request->getUri();

        if ($uri == '/') {
            return \Action\Welcome::class;
        }

        return ucwords('\Action' . str_replace('/', '\\', rtrim($uri, '/')), '\\');
    }

}