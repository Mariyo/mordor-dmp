<?php

class Request implements Interfaces\IRequest
{

    /**
     * @var array
     */
    protected $requestData;

    public function __construct(array $requestData)
    {
        $this->requestData = $requestData;
    }

    public function getUri()
    {
        return isset($this->requestData['REQUEST_URI']) ? preg_replace('~\?.*~', '',
            $this->requestData['REQUEST_URI']) : false;
    }

}