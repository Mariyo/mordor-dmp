<?php

class Response implements Interfaces\IResponse
{

    /**
     * @var string
     */
    protected $body;

    /**
     * @param string $body
     * @return $this
     */
    public function setBody(string $body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return $this
     */
    public function sendHeaders()
    {
        // TODO: Implement sendHeaders() method.
        return $this;
    }

    /**
     * @return $this
     */
    public function sendContent()
    {
        echo $this->getBody();
        return $this;
    }
}