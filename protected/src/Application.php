<?php

class Application implements Interfaces\IApplication
{

    /**
     * @var Interfaces\IRouter
     */
    protected $router;

    /**
     * This should be replaced by Dependency injection in future.
     * @var PDO
     */
    protected $pdo;

    /**
     * Application constructor.
     * @param Interfaces\IRouter $router
     * @param PDO $pdo
     */
    public function __construct(Interfaces\IRouter $router, PDO $pdo)
    {
        $this->router = $router;
        $this->pdo = $pdo;
        $this->dbcheck();
    }

    /**
     * @param \Interfaces\IRequest $request
     */
    public function run(Interfaces\IRequest $request)
    {
        $actionClass = $this->router->route($request);
        $action = $this->actionFactory($actionClass, $request, $this->pdo);
        $response = $action->run();
        $response->sendHeaders();
        $response->sendContent();
    }

    /**
     * TODO: Should be replaced by Dependency injection...
     *
     * @param $actionClass
     * @param \Interfaces\IRequest $request
     * @param PDO $pdo
     * @return \Interfaces\IAction
     */
    protected function actionFactory($actionClass, Interfaces\IRequest $request, PDO $pdo): \Interfaces\IAction
    {
        return new $actionClass($request, $pdo);
    }

    /**
     *
     */
    protected function dbcheck()
    {
        foreach (['creatures', 'crimes', 'notes'] as $table) {
            $this->pdo->query("SELECT 1 FROM $table");

            if ($this->pdo->errorCode() == '42S02') {
                $sql = file_get_contents("protected/sql/$table.sql");
                $this->pdo->query($sql);
            }
        }
    }

}