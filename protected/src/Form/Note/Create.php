<?php

namespace Form\Note;

use Interfaces\IForm;

class Create implements IForm
{

    /**
     * @return array
     */
    public function validate(): array
    {
        $errors = [];
        $values = $this->values();

        if (empty($values['note'])) {
            $errors[] = 'Note is required';
        }

        if (strlen($values['note']) > 10000) {
            $errors[] = 'Note is too long';
        }

        return $errors;
    }

    /**
     * @return array
     */
    public function values(): array
    {
        return $_POST;
    }

    /**
     * @return array
     */
    protected function value($key)
    {
        $values = $this->values();
        return isset($values[$key]) ? $values[$key] : null;
    }

    /**
     * @return bool
     */
    public function isSubmitted(): bool
    {
        return !empty($_POST['create_note']);
    }
}