<?php

namespace Form\Creature;

use Dao\Creature;
use DateTime;
use Exception;
use Interfaces\IForm;

class Create implements IForm
{

    /**
     * @return array
     */
    public function validate(): array
    {
        $errors = [];
        $values = $this->values();

        if (empty($values['name'])) {
            $errors[] = 'Name is required';
        }

        if (strlen($values['name']) > 60) {
            $errors[] = 'Name is too long';
        }

        if (empty($values['gender'])) {
            $errors[] = 'Gender is required';
        }

        if (!array_key_exists($values['gender'], Creature::GENDERS)) {
            $errors[] = 'Gender is not valid';
        }

        if (empty($values['birthplace'])) {
            $errors[] = 'Birthplace is required';
        }

        if (strlen($values['birthplace']) > 50) {
            $errors[] = 'Birthplace is too long';
        }

        if (empty($values['birthdate'])) {
            $errors[] = 'Birthdate is required';
        } else {
            try {
                $birthdate = new DateTime($values['birthdate']);

                if ($birthdate > new DateTime()) {
                    throw new Exception('Birthdate must be in past');
                }
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        if (empty($values['race'])) {
            $errors[] = 'Race is required';
        }

        if (!array_key_exists($values['race'], Creature::RACES)) {
            $errors[] = 'Race is not valid';
        }

        return $errors;
    }

    /**
     * @return array
     */
    public function values(): array
    {
        $values = $_POST;
        $values['carrier'] = !empty($values['carrier']) ? 1 : 0;
        $values['enslaved'] = !empty($values['enslaved']) ? 1 : 0;
        $values['gender'] = !empty($values['gender']) ? (int)$values['gender'] : 0;
        $values['race'] = !empty($values['race']) ? (int)$values['race'] : 0;
        return $values;
    }

    /**
     * @return array
     */
    protected function value($key)
    {
        $values = $this->values();
        return isset($values[$key]) ? $values[$key] : null;
    }

    /**
     * @return bool
     */
    public function isSubmitted(): bool
    {
        return !empty($_POST['create_creature']);
    }
}