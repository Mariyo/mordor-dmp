<html>
<head>
    <title><?= isset($title) ? $title : 'No title' ?></title>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: .5rem 2rem;
        }

        table.create th {
            text-align: right;
        }

        table.create td.centered {
            text-align: center;
        }
    </style>
</head>
<body>
<form method="post">
    <h1><?= isset($title) ? $title : 'Insert password' ?></h1>
    <table>
        <tr>
            <th>Password</th>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <th colspan="2" class="centered">
                <button type="submit" name="password_create_verify">Verify password</button>
            </th>
        </tr>
    </table>
</form>
</body>
</html>