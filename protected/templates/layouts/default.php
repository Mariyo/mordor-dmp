<html>
<head>
    <title><?= isset($title) ? $title : 'No title' ?></title>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: .5rem 2rem;
        }

        table.create th {
            text-align: right;
        }

        table.create td.centered {
            text-align: center;
        }

        a.orderedby {
            color: #f00;
        }

        a.notorderedby {
            color: #00f;
        }
    </style>
</head>
<body>
<div>
    <a href="/">Home</a> |
    <a href="/creature/create">Create creature</a>
</div>
<div>
    <?php
    if (!empty($errors)) {
        ?>
        <ul style="color: #f00; border: 1px #f00 solid;">
            <?php
            foreach ($errors as $error) {
                ?>
                <li><?= $error ?></li>
                <?php
            }
            ?>
        </ul>
        <?php
    }
    ?>
    <?= isset($content) ? $content : '' ?>
</div>
</body>
</html>