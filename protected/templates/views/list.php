<h1>List of creatures</h1>

<form method="get">
    <table>
        <tr>
            <th>Crimes not punished</th>
            <td><input type="number" name="crimes" value="<?= !empty($crimesSelected) ? $crimesSelected : '' ?>"></td>
        </tr>
        <tr>
            <th>Race</th>
            <td>
                <select name="race">
                    <option value="">-</option>
                    <?php
                    foreach (Dao\Creature::RACES as $raceKey => $race) {
                        ?>
                        <option value="<?= $raceKey ?>" <?= $raceKey == $raceSelected ? 'selected' : '' ?>><?= $race ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th colspan="2" class="centered">
                <input type="hidden" name="order" value="<?= $order ?>">
                <input type="hidden" name="orderby" value="<?= $orderby ?>">
                <button type="submit" name="filter">Filter</button>
            </th>
        </tr>
    </table>
</form>

<?php

if (empty($creatures)) {
    ?>
    There is no creature found in DB.
    <?php
    return;
}

function orderbyLink($column, $orderby, $order)
{
    $get = $_GET;
    $get['orderby'] = $column;

    if ($column == $orderby and $order != 'desc') {
        $get['order'] = 'desc';
    } else {
        unset($get['order']);
    }

    return '?' . http_build_query($get);
}

function orderbyClass($column, $orderby)
{
    return $column == $orderby ? 'orderedby' : 'notorderedby';
}

function orderbyArrow($column, $order)
{
    return $order == 'desc' ? '&nbsp;↑' : '&nbsp;↓';
}

?>

<table>
    <tr>
        <th>
            <a href="<?= orderbyLink('id', $orderby, $order) ?>"
               class="<?= orderbyClass('id', $orderby) ?>">ID<?= orderbyArrow('id', $order) ?></a>
        </th>
        <th>
            <a href="<?= orderbyLink('name', $orderby, $order) ?>"
               class="<?= orderbyClass('name', $orderby) ?>">Name<?= orderbyArrow('name', $order) ?></a>
        </th>
        <th>Gender</th>
        <th>Birthplace</th>
        <th>
            <a href="<?= orderbyLink('birthdate', $orderby, $order) ?>"
               class="<?= orderbyClass('birthdate', $orderby) ?>">Birthdate<?= orderbyArrow('birthdate', $order) ?></a>
        </th>
        <th>Ever carried The Ring?</th>
        <th>Enslaved by Sauron?</th>
        <th>Race</th>
    </tr>
    <?php
    foreach ($creatures as $creature) {
        ?>
        <tr>
            <td><?= $creature->id ?></td>
            <td><?= htmlspecialchars($creature->name, ENT_QUOTES, 'UTF-8') ?></td>
            <td><?= htmlspecialchars($creature->gender, ENT_QUOTES, 'UTF-8') ?></td>
            <td><?= htmlspecialchars($creature->birthplace, ENT_QUOTES, 'UTF-8') ?></td>
            <td><?= htmlspecialchars($creature->birthdate, ENT_QUOTES, 'UTF-8') ?></td>
            <td><?= !empty($creature->carrier) ? 'Yes' : 'No' ?></td>
            <td><?= !empty($creature->enslaved) ? 'Yes' : 'No' ?></td>
            <td><?= htmlspecialchars($creature->race, ENT_QUOTES, 'UTF-8') ?></td>
            <td>
                <?php
                $get = $_GET;
                $get['remove'] = $creature->id;
                ?>
                <a href="?<?= http_build_query($get) ?>">Decease</a>
            </td>
        </tr>
        <tr>
            <td>Crimes</td>
            <td colspan="7">
                <?php
                if (!empty($crimes[$creature->id])) {
                    foreach ($crimes[$creature->id] as $crime) {
                        ?>
                        <li>
                            (<?= $crime->date ?>)
                            <?= $crime->punished ? 'Punished' : 'Not Punished' ?>
                            <?= htmlspecialchars($crime->note, ENT_QUOTES, 'UTF-8') ?>
                        </li>
                        <?php
                    }
                } else {
                    ?>
                    No crimes
                    <?php
                }
                ?>
            </td>
            <td><a href="/crime/create?creature=<?= $creature->id ?>&redirect=<?= $redirect ?>">Add&nbsp;crime</a></td>
        </tr>
        <tr>
            <td>Notes</td>
            <td colspan="7">
                <?php
                if (!empty($notes[$creature->id])) {
                    foreach ($notes[$creature->id] as $note) {
                        ?>
                        <li>(<?= $note->date ?>) <?= htmlspecialchars($note->note, ENT_QUOTES, 'UTF-8') ?></li>
                        <?php
                    }
                } else {
                    ?>
                    No notes
                    <?php
                }
                ?>
            </td>
            <td>
                <a href="/note/create?creature=<?= $creature->id ?>&redirect=<?= $redirect ?>">Add&nbsp;note</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
