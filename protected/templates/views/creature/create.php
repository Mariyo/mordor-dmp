<h1>New creature</h1>

<form method="post">
    <table class="create">
        <tr>
            <th>Name:</th>
            <td><input type="text" name="name" maxlength="60"></td>
        </tr>
        <tr>
            <th>Gender:</th>
            <td>
                <select name="gender">
                    <?php
                    foreach (Dao\Creature::GENDERS as $genderKey => $gender) {
                        ?>
                        <option value="<?= $genderKey ?>"><?= $gender ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>Birthplace:</th>
            <td><input type="text" name="birthplace" maxlength="50"></td>
        </tr>
        <tr>
            <th>Birthdate:</th>
            <td><input type="date" name="birthdate"></td>
        </tr>
        <tr>
            <th>Ever carried The Ring?:</th>
            <td><input type="checkbox" name="carrier" value="1"></td>
        </tr>
        <tr>
            <th>Enslaved by Sauron?:</th>
            <td><input type="checkbox" name="enslaved" value="1"></td>
        </tr>
        <tr>
            <th>Race:</th>
            <td>
                <select name="race">
                    <?php
                    foreach (Dao\Creature::RACES as $raceKey => $race) {
                        ?>
                        <option value="<?= $raceKey ?>"><?= $race ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="centered">
                <input type="hidden" name="create_creature" value="1">
                <button type="submit" name="create_creature_submit">Create creature</button>
            </td>
        </tr>
    </table>
</form>