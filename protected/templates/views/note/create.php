<h1>New note</h1>

<form method="post">
    <table class="create">
        <tr>
            <th>Note:</th>
            <td><textarea name="note"></textarea></td>
        </tr>
        <tr>
            <td colspan="2" class="centered">
                <input type="hidden" name="create_note" value="1">
                <button type="submit" name="create_note_submit">Create note</button>
            </td>
        </tr>
    </table>
</form>