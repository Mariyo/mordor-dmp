<h1>New crime</h1>

<form method="post">
    <table class="create">
        <tr>
            <th>Note:</th>
            <td><textarea name="note"></textarea></td>
        </tr>
        <tr>
            <th>Punished?:</th>
            <td><input type="checkbox" name="punished" value="1"></td>
        </tr>
        <tr>
            <td colspan="2" class="centered">
                <input type="hidden" name="create_crime" value="1">
                <button type="submit" name="create_crime_submit">Create crime</button>
            </td>
        </tr>
    </table>
</form>