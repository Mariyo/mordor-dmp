<?php

/**
 * Session start
 */
session_id() or session_start();

/**
 * PSR-0 autoloader.
 */
spl_autoload_register(function ($className) {
    $className = ltrim($className, '\\');
    $fileName = 'protected/src/';

    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName .= str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    if (realpath($fileName) == false) {
        throw new Exception('Class not found ' . $fileName);
    }

    require $fileName;
});

/**
 * TODO: should be changed into more "lazy", kind of service...
 * Config
 */
function config()
{
    $localPath = 'protected/config-local.php';
    return realpath($localPath) ? require $localPath : require 'protected/config.php';
}

/**
 * Register router for the Application
 */
$router = new Router();

/**
 * Request
 */
$request = new Request($_SERVER);

/**
 * PDO service.
 */
$dbConfig = config()['database'];
$pdo = new PDO('mysql:host=' . $dbConfig['host'] . ';dbname=' . $dbConfig['name'] . ';charset=' . $dbConfig['charset'],
    $dbConfig['user'], $dbConfig['pass']);

/**
 * Call application
 */
$app = new Application($router, $pdo);
$app->run($request);